#!/bin/bash

yum install centos-release-scl
yum install devtoolset-7 llvm-toolset-7-cmake
scl enable devtoolset-7 llvm-toolset-7 bash

FROM takeshitam/centos-ja
MAINTAINER takeshitam <mst.take@gmail.com>

ENV JUMANPP_VERSION=v2.0.0-rc2 \
    KNP_VERSION=

RUN curl -L -O https://github.com/ku-nlp/jumanpp/releases/download/${JUMANPP_VERSION}/jumanpp-${JUMANPP_VERSION}.tar.xz
RUN tar xf jumanpp-${JUMANPP_VERSION}.tar.xz 
RUN mkdir jumanpp-${JUMANPP_VERSION}/build
RUN cd jumanpp-${JUMANPP_VERSION}/build \
    && cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
    && make install -j4


